# Paul's dotfiles

**WARNING**: To do any of this your machine needs to be authorised by BitBucket. Generate an SSH key via `ssh-keygen -t rsa`.

Run this command:

    curl -Lks https://bitbucket.org/paul_sagekey/dotfiles/raw/master/.dotfiles-extras/dotfiles-setup | /bin/bash

Now the dotfiles will exist! Use the `dotfiles` command just like you would Git, e.g.:

    $ dotfiles add .nanorc
    $ dotfiles commit -m "Added Nano config"
    $ dotfiles push

You can find the dotfiles installation script in `~/.dotfiles-extras`. Feel free to delete this README and `~/.dotfiles-extras`, but just don't push those deletions!